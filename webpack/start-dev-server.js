const path = require('path');
const mkpath = require('mkpath');
const webpack = require('webpack');

// globals
global.BUILD__STARTED = Date.now();

// Chain load the build process with promise without polluting scope
prepareToBuild()
    .then(buildClient)
    .then(startDevServer)
    .then(reportBuildStatus)
    .catch((err) => {
    console.log(new Error(err));
});

/**
 * Prepare what necessary to build
 * @returns {Promise}
 */
function prepareToBuild() {
    return new Promise((resolve) => {
        const buildConfig = require('./webpack-build-config'); // eslint-disable-line global-require,max-len
        buildConfig.DEVELOPMENT = true;;
        // get package.json, config.json
        const packageJSON = require(path.join(buildConfig.ABS_PATH, 'package.json')); // eslint-disable-line global-require,max-len

        // ensure required folders
        mkpath.sync(buildConfig.DEV_PATH);
        mkpath.sync(buildConfig.CACHE_PATH);

        resolve({buildConfig, packageJSON});
    });
}

/**
 * @param buildConfig
 * @param packageJSON
 * @returns {Promise}
 */
function buildClient({buildConfig, packageJSON}) {
    return new Promise((resolve) => {
        let webpackConfig;

        try {
            const clientConfig = require('./webpack.config.js'); // eslint-disable-line global-require
            webpackConfig = clientConfig({buildConfig, packageJSON});
        } catch (e) {
            throw e;
        }

        // build with webpack (watch mode)
        const clientCompiler = webpack(webpackConfig);

        clientCompiler.plugin('done', (stats) => {
            console.log(stats.toString({
                chunks: false,
                colors: true,
            }));
        });

        resolve({buildConfig, compiler: clientCompiler, packageJSON});
    });
}

/**
 * @param buildConfig
 * @param compiler webpack build instance
 * @param packageJSON
 * @returns {Promise}
 */
function startDevServer({buildConfig, compiler, packageJSON}) {
    try {
        return require('./webserver')({buildConfig, compiler, packageJSON}); // eslint-disable-line global-require,max-len
    } catch (e) {
        throw e;
    }
}

/**
 * @param buildConfig
 * @param packageJSON
 */
function reportBuildStatus({buildConfig, packageJSON}) {
    // done
    console.info('----\n==> ✅  %s is up and running (%dms).', packageJSON.name, (Date.now() - global.BUILD__STARTED));
    if (buildConfig.LOCAL_DEVELOPMENT) {
    }
    else {
        console.info('==> ✅  Open http://%s:%s in a browser to view the app.',
            buildConfig.DEV_SERVER_ADDRESS, buildConfig.DEV_SERVER_PORT);
    }
}
