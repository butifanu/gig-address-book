const path = require('path');
const fs = require('fs');

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');

// default build config constants
const buildConfigDefault = require(`${__dirname}/webpack-build-config`);

module.exports = ({ buildConfig }) => {

    const babelConfig = JSON.parse(fs.readFileSync(path.resolve(`${buildConfigDefault.ABS_PATH}/.babelrc`), 'utf-8')); // eslint-disable-line max-len
    const jsLoaderConfig = JSON.stringify(babelConfig);
    const jsLoaders = [`babel-loader?${jsLoaderConfig}`];

    const buildOutputPath = buildConfig.DEVELOPMENT ? buildConfig.DEV_PATH : buildConfig.DIST_PATH;
    const enableSourceMap = buildConfig.DEVELOPMENT ?
        buildConfig.WEBPACK_ENABLE_SOURCEMAPS_DEV : buildConfig.WEBPACK_ENABLE_SOURCEMAPS_BUILD;

    // path used in module js loaders include property
    const jsModuleIncludePath = buildConfig.SRC_PATH;
    // path used isn module style loaders include property
    const styleModuleIncludePath = path.resolve(buildConfig.SRC_PATH, 'assets/styles');


    // loaders configs
    const cssLoaderConfig = {
        importLoaders: 2,
        sourceMap: enableSourceMap,
    };

    const devOnlyPlugins = [
        // hot module replacement
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
    ];

    // we don't have a prod builder, but can be added easily
    const prodOnlyPlugins = [
        // empty the dist folder
        new CleanPlugin([buildConfig.DIST_PATH], { root: buildConfig.ABS_PATH, verbose: false }),
        // extract CSS files
        new ExtractTextPlugin(path.join('./', buildConfig.STATIC_RELATIVE_CSS, '/[name]-[chunkhash].css'), { allChunks: true }),
        // optimizations
        new webpack.optimize.UglifyJsPlugin({
            uglifyOptions: {
                compress: { warnings: false },
            },
            sourceMap: buildConfig.WEBPACK_ENABLE_SOURCEMAPS_BUILD,
        }),
    ];



    const config = {
        target: 'web',
        context: buildConfig.ABS_PATH,
        entry: {
            hmr: [
                'react-hot-loader/patch',
                'webpack-dev-server/client?http://localhost:3000',
                'webpack/hot/only-dev-server'
            ],
            main: path.resolve(buildConfig.SRC_PATH, 'main.js'),
        },
        output: {
            path: buildOutputPath,
            filename: path.join(buildConfig.STATIC_RELATIVE_JS, '/[name].[chunkhash].js'),
            sourceMapFilename: path.join(buildConfig.STATIC_RELATIVE_JS, '/[name].js.map'),
            chunkFilename: path.join(buildConfig.STATIC_RELATIVE_JS, '/[name].[chunkhash].js'),
            publicPath: buildConfig.WEBPACK_PUBLIC_PATH,
        },
        module: {
            rules: [
                {
                    test: /\.js$/i,
                    include: jsModuleIncludePath,
                    use: jsLoaders,
                },
                {
                    test: /\.(scss|css)/i,
                    include: styleModuleIncludePath,
                    use: [
                        { loader: 'style-loader' },
                        {
                            loader: 'css-loader',
                            options: cssLoaderConfig,
                        },
                        { loader: 'sass-loader' },
                    ],
                },
                {
                    test: /\.(woff|woff(2))$/,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {}
                        }
                    ]
                }
            ],
        },
        resolve: {
            extensions: ['.json', '.js', '.css', '.scss', '.woff', '.woff2'],
        },

        devtool: enableSourceMap ? 'cheap-module-source-map' : false,
        plugins: [
            // index.html
            new HtmlWebpackPlugin({
                template: path.join(buildConfig.SRC_PATH, 'index.html'),
                filename: path.join(buildOutputPath, 'index.html'),
                chunksSortMode: 'dependency',
                minify: buildConfig.DEVELOPMENT ? undefined : {
                    removeComments: true,
                    preserveLineBreaks: false,
                    collapseWhitespace: true,
                    minifyCSS: true,
                },

            }),

            new webpack.optimize.CommonsChunkPlugin({
                name: "commons",
                filename: path.join(buildConfig.STATIC_RELATIVE_JS, 'commons.js'),
                minChunks: 2,
            }),

        ].concat(buildConfig.DEVELOPMENT ? devOnlyPlugins : prodOnlyPlugins),
    };

    return config;
};