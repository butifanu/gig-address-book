const WebpackDevServer = require('webpack-dev-server');

module.exports = ({ buildConfig, compiler, packageJSON }) => {
    return new Promise((resolve) => {

        compiler.plugin('done', () => {
            resolve({ buildConfig, packageJSON });
        });

        const server = new WebpackDevServer(compiler, {
            contentBase: buildConfig.DEV_PATH,
            hot: true,
            // This is handy if you are using a html5 router.
            historyApiFallback: true,
            // Set this if you want to enable gzip compression for styles
            noInfo: true,
            compress: true,
            stats: { colors: true },
        });

        server.listen(buildConfig.DEV_SERVER_PORT, buildConfig.DEV_SERVER_ADDRESS, () => {});
    });
};
