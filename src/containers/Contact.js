import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import AddContactForm from '../components/add-contact-form';
import {
    storeIdConst,
    editContactEntry,
    removeContactEntry,
} from '../store/store';
import { validate } from '../utils/validation/validate';

/**
 * @name Contact
 * @description This is a container with all the logic needed to add, edit or remove a contact.
 *              This container is used only with the Route component from react-router, because it needs the data
 *              provided through its 'location' prop.
 * @example
 * <Route exact path="/contact/:id" component={Contact} />
 */
class Contact extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        contacts: PropTypes.array,
        countries: PropTypes.array,
        editContactEntry: PropTypes.func,
        removeContactEntry: PropTypes.func,
    }

    constructor(props) {
        super(props);
        this.state = {
            contactData: props.location.state.contact,
            editing: false,
            showAddNewInput: false,
            error: {},
        };
    }

    componentWillReceiveProps(nextProps) {
        const prevContact = this.props.location.state.contact;
        const nextContact = nextProps.location.state.contact;

        // update the contact data as the id in the URL changes
        if (prevContact.id !== nextContact.id) {
            this.setState({
                contactData: nextContact,
            });
        }

        // close the editing form when changing contact entries
        const currentPath = this.props.location.pathname;
        const nextPath = nextProps.location.pathname;
        if (currentPath !== nextPath) {
            this.setState({
                editing: false,
            });
        }

        // update the contacts list if contacts are removed
        if (this.props.contacts.length !== nextProps.contacts.length) {
            this.props.history.push('/');
        }
    }

    handleEditContact = (e) => {
        e.preventDefault();
        // save the new data to the storage
        this.validateAllFields(this.state.contactData);
    }

    validateAllFields = (contactObj) => {
        return Object.keys(contactObj).filter((key) => key !== 'id' && key !== 'country').forEach((key, i) => {
            setTimeout(() => {
                validate(key, contactObj[key], this.setState.bind(this), this.state.error);
                const hasNoErrors = Object.keys(this.state.error).every((key) => {
                    return this.state.error[key].message === '';
                });
                this.setState(() => {
                    return { hasNoErrors };
                });
                if (hasNoErrors && Object.keys(contactObj).includes('id')) {
                    this.setState({
                        editing: false,
                    });
                    // save the new edited entry
                    this.props.editContactEntry(contactObj);
                    // close the contact edit mode
                    return this.props.history.push('/');
                }

                // if not valid, keep the errors
                return false;
            }, i);
        });
    }

    handleEditField = (e, field) => {
        let updatedContactData;

        // handle differently the country field
        if (field === 'country') {
            updatedContactData = Object.assign({}, this.state.contactData, {
                [field]: {
                    code: e.target.value,
                    name: this.props.countries.filter((country) => country.code === e.target.value)[0].name,
                },
            });
        } else {
            updatedContactData = Object.assign({}, this.state.contactData, { [field]: e.target.value });
        }

        // update the contact with the new values
        this.setState({ contactData: updatedContactData });

        // get the value again, after the state batching
        setTimeout(() => {
            validate(field, this.state.contactData[field], this.setState.bind(this), this.state.error);
        }, 0);
    }

    showAddNewContact = (e) => {
        e.preventDefault();
        this.setState({
            showAddNewInput: !this.state.showAddNewInput,
        });
    }

    toggleEditButton = (e) => {
        e.preventDefault();
        // reset errors if not in editing mode
        if (this.state.editing) {
            this.setState({
                error: {},
                // if we close the edit mode, reset what we entered into the inputs
                contactData: this.props.location.state.contact,
            });
        }

        // toggle the editing mode
        this.setState({
            editing: !this.state.editing,
        });
    }

    handleRemoveContact = (e, id) => {
        e.preventDefault();
        if (window.confirm('Are you sure you want to delete this contact?') === true) { // eslint-disable-line no-alert
            this.props.removeContactEntry(id);
            // close the contact edit mode
            return this.props.history.push('/');
        }

        // if we press 'cancel', do nothing
        return false;
    }

    renderInput = (inputName, inputValue, type = 'text', placeholder) => {
        if (type !== 'select') {
            return (
                <input
                    type={type}
                    name={inputName}
                    id={inputName}
                    defaultValue={inputValue}
                    placeholder={placeholder}
                    onBlur={(e) => this.handleEditField(e, inputName)}
                />
            );
        }

        return (
            <select
                id={inputName}
                onChange={(e) => this.handleEditField(e, inputName)}
                defaultValue={inputValue.code}
            >
                {this.props.countries.map((country, i) => {
                    return (
                        <option value={country.code} key={i}>{country.name}</option>
                    );
                })}
            </select>
        );
    }

    renderAddContactForm = () => { // renders the form for adding new contact
        return (
            <AddContactForm countries={this.props.countries} />
        );
    }

    render() {
        const {
            id, lastName, firstName, email, country,
        } = this.state.contactData;
        const editLabel = this.state.editing ? 'Close Edit' : 'Edit';
        const isEditing = this.state.editing;

        return (
            <div className="contact-container">
                <h3 className="header-subtitle edit-contact">Edit Contact</h3>
                <Link to="/" className="go-back" href="/">&#8592; go back</Link>
                <form className="edit-form" onSubmit={this.handleEditContact}>
                    <fieldset>
                        <button className="edit-contact" onClick={this.toggleEditButton}>{editLabel}</button>
                        {!this.state.editing &&
                        (
                            <div className="contact-actions">
                                <button
                                    className="remove-contact"
                                    onClick={(e) => this.handleRemoveContact(e, id)}
                                >
                                    Remove Contact
                                </button>
                            </div>
                        )}
                        <ul className="contact-details">
                            <li className="contact-details-item contact-first-name">
                                <span className="contact-label">First Name: </span>
                                {isEditing ?
                                    this.renderInput('firstName', firstName) :
                                    <span className="contact-value">{firstName}</span>}
                            </li>
                            <li className="contact-details-item contact-last-name">
                                <span className="contact-label">Last Name: </span>
                                {isEditing ?
                                    this.renderInput('lastName', lastName) :
                                    <span className="contact-value">{lastName}</span>}
                            </li>
                            <li className="contact-details-item contact-email">
                                <span className="contact-label">Email: </span>
                                {isEditing ?
                                    this.renderInput('email', email, 'email') :
                                    <span className="contact-value">{email}</span>}
                            </li>
                            <li className="contact-details-item contact-country">
                                <span className="contact-label">Country: </span>
                                {isEditing ?
                                    this.renderInput('country', country, 'select') :
                                    <span className="contact-value">{country.name}</span>}
                            </li>
                        </ul>
                        <ul className="error-messages">
                            {Object.keys(this.state.error).map((key, i) => {
                                return (
                                    <li className="error-item" key={i}>
                                        <span className="error-message">{this.state.error[key].message}</span>
                                    </li>
                                );
                            })}
                        </ul>
                        {this.state.editing &&
                        <button type="submit" className="save-contact">Save</button>}
                    </fieldset>
                </form>
                {this.state.showAddNewInput && this.renderAddContactForm()}
            </div>
        );
    }
}

export default connect(
    (state) => ({
        countries: state[storeIdConst.STORE_ID].countries,
        contacts: state[storeIdConst.STORE_ID].contacts,
    }),
    (dispatch) => bindActionCreators({
        editContactEntry,
        removeContactEntry,
    }, dispatch),
)(Contact);
