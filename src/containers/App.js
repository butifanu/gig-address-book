import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Route } from 'react-router-dom';
import countries from 'country-list';

import AddContactForm from '../components/add-contact-form';
import ShowContacts from '../components/show-contacts';
import Contact from './Contact';

import { mockContacts } from '../utils/storage/mock-contacts';
import { addToStorage, getFromStorage } from '../utils/storage/storage';
import { appConfig } from '../configs/app-config';
import { storeIdConst, getCurrentContacts, addMockContacts, saveCountries } from '../store/store';

import '../assets/styles/index.scss';

/**
 * @name App
 * @description This is the main container for our app in which we render everything we need.
 * @example
 * <App />
 */
class App extends Component {
    static propTypes = {
        location: PropTypes.object,
        history: PropTypes.object,
        getCurrentContacts: PropTypes.func.isRequired,
        addMockContacts: PropTypes.func.isRequired,
        saveCountries: PropTypes.func,
        contacts: PropTypes.array,
        countries: PropTypes.array,
    }

    constructor() {
        super();
        this.state = {
            allContacts: [],
            showAddNewInput: false,
            countries: countries().getData(),
        };
    }

    componentWillMount() {
        // init the localStorage collection
        addToStorage('contacts', []);

        if (!this.props.contacts.length && Object.prototype.toString.call(this.props.contacts) === '[object Array]' &&
            getFromStorage('contacts') && getFromStorage('contacts').length) {
            // get the current contacts if present
            this.props.getCurrentContacts();
        } else if (!this.props.contacts.length && !this.state.showAddNewInput && appConfig.useMocks) {
            // add some mock contacts to start playing with
            this.props.addMockContacts(mockContacts);
        }

        // save the countries to the store
        this.props.saveCountries(this.state.countries);


        // if we don't have the contacts yet and the user tries to navigate to /contact/:id, it will not work
        if (!this.props.contacts.length) {
            this.props.history.push('/');
        }
    }

    componentWillReceiveProps(nextProps) {
        // update the contacts list
        if (this.props.contacts !== nextProps.contacts) {
            this.setState({
                allContacts: nextProps.contacts,
            });
        }

        // close the add new contact form after previously adding a contact
        if (this.props.contacts.length !== nextProps.contacts.length) {
            this.setState({
                showAddNewInput: false,
            });
        }

        // if we change the location path and the add new contact form is open, automatically close it
        if (this.props.location.pathname !== nextProps.location.pathname && this.state.showAddNewInput) {
            this.setState({
                showAddNewInput: false,
            });
        }
    }

    showAddNewContact = (e) => {
        e.preventDefault();
        this.setState({
            showAddNewInput: !this.state.showAddNewInput,
        });
    }

    renderAddContactForm = () => {
        return (
            <AddContactForm countries={this.props.countries} />
        );
    }

    render() {
        const addContactLabel = !this.state.showAddNewInput ? 'Add New Contact' : 'Close Add New Contact';

        if (!this.props.contacts.length) { // the view for when we don't have entries
            return (
                <div className="address-book-container no-entries">
                    <h2 className="no-contacts">You have no contacts.. :-(</h2>
                    <div className="add-new-contact-wrapper">
                        <button className="add-new-contact" onClick={this.showAddNewContact}>{addContactLabel}</button>
                    </div>
                    {this.state.showAddNewInput && this.renderAddContactForm()}
                </div>
            );
        }

        return (
            <div className="address-book-container">
                <div className="address-book">
                    <h2 className="header-title">All Address Book Entries</h2>
                    <button className="add-new-contact" onClick={this.showAddNewContact}>
                        {addContactLabel}
                    </button>
                    {this.state.showAddNewInput && this.renderAddContactForm()}
                    <Route exact path="/contact/:id" component={Contact} />
                    <ShowContacts contacts={this.state.allContacts} />
                </div>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        countries: state[storeIdConst.STORE_ID].countries,
        contacts: state[storeIdConst.STORE_ID].contacts,
    }),
    (dispatch) => bindActionCreators({
        getCurrentContacts,
        addMockContacts,
        saveCountries,
    }, dispatch),
)(App);
