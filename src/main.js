import 'react-hot-loader/patch';

import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import { getStore, init as storeInit } from './store/create-store';
import { appReducer } from './store/store';

import EntryPoint from './containers/EntryPoint';

/**
 * @name renderApp
 * @description A wrapper that's used to initiate the ReactDOM.render method on a given node.
 * @param {object} Component - a React valid component that will be rendered; in this case, the main app.
 * @example
 * renderApp(EntryPoint);
 */
const renderApp = (Component, node = 'App') => {
    const reducers = {
        appReducer,
    };
    storeInit(reducers);
    const store = getStore();

    ReactDOM.render(
        <AppContainer>
            <Provider store={store}>
                <Component />
            </Provider>
        </AppContainer>,
        document.getElementById(node),
    );
};

renderApp(EntryPoint);

// Webpack Hot Module Replacement API
if (module.hot) {
    module.hot.accept('./containers/EntryPoint', () => {
        const NextRootContainer = require('./containers/EntryPoint'); // eslint-disable-line global-require
        renderApp(NextRootContainer);
    });
}
