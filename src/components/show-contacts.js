import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

/**
 * @name ShowContacts
 * @description Renders a list with all the contacts we have in store at the moment.
 * @param {array} contacts - A list with all the contacts to be rendered
 * @example
 * <ShowContacts contacts={this.props.contacts} />
 */
export default class ShowContacts extends Component {
    static propTypes = {
        contacts: PropTypes.array,
    }

    constructor(props) {
        super(props);
        this.state = {
            contacts: props.contacts.sort(this.sortById) || [],
            sortedType: 'oldest',
        };
    }

    componentWillReceiveProps(nextProps) {
        // update the entries
        if (this.props.contacts !== nextProps.contacts) {
            this.setState({
                contacts: nextProps.contacts,
            }, () => {
                // we sort the entries again, with the new edits, additions or removals
                this.handleSort(null, this.state.sortedType);
            });
        }
    }

    sortById = (a, b) => {
        if (a.id < b.id) {
            return -1;
        }

        if (a.id > b.id) {
            return 1;
        }

        return 0;
    }

    handleSort = (e, type) => {
        if (e) {
            e.preventDefault();
        }
        // sort by the oldest, newest, alphabetically and by country
        switch (type) {
            case 'oldest':
                this.setState({
                    contacts: this.state.contacts.sort(this.sortById),
                    sortedType: type,
                });
                break;
            case 'newest':
                this.setState({
                    contacts: this.state.contacts.sort(this.sortById).reverse(),
                    sortedType: type,
                });
                break;
            case 'alphabetically':
                this.setState({
                    contacts: this.state.contacts.sort(this.sortById).sort((a, b) => {
                        if (a.firstName.toLowerCase() < b.firstName.toLowerCase()) {
                            return -1;
                        }
                        if (a.firstName.toLowerCase() > b.firstName.toLowerCase()) {
                            return 1;
                        }

                        return 0;
                    }),
                    sortedType: type,
                });
                break;
            case 'country':
                this.setState({
                    contacts: this.state.contacts.sort(this.sortById).sort((a, b) => {
                        if (a.country.name.toLowerCase() < b.country.name.toLowerCase()) {
                            return -1;
                        }
                        if (a.country.name.toLowerCase() > b.country.name.toLowerCase()) {
                            return 1;
                        }

                        return 0;
                    }),
                    sortedType: type,
                });
                break;
            default:
                this.setState({
                    contacts: this.state.contacts.sort(this.sortById),
                    sortedType: type,
                });
        }
    }

    sortedByCriteria = (criteria, item, attribute) => {
        return this.state.sortedType === criteria ?
            <span className="sorted-by-criteria">{item[attribute]}</span> : item[attribute];
    }

    render() {
        /* eslint-disable jsx-a11y/anchor-is-valid */
        return (
            <div className="contact-list-wrapper">
                <div className="sorting-wrapper">
                    <span className="sort-label">sort by:</span>
                    <ul className="sorting-options">
                        <li className="sorting-item oldest">
                            {this.state.sortedType === 'oldest' ?
                                <span className="sorting-option">oldest &#8595;</span> :
                                <a
                                    href="#"
                                    className="sorting-anchor"
                                    onClick={(e) => this.handleSort(e, 'oldest')}
                                    title="Sort by the Oldest"
                                >
                                    oldest &#8595;
                                </a>
                            }
                        </li>
                        <li className="sorting-item newest">
                            {this.state.sortedType === 'newest' ?
                                <span className="sorting-option">newest &#8593;</span> :
                                <a
                                    href="#"
                                    className="sorting-anchor"
                                    onClick={(e) => this.handleSort(e, 'newest')}
                                    title="Sort by the Newest"
                                >
                                    newest &#8593;
                                </a>
                            }
                        </li>
                        <li className="sorting-item alphabetically">
                            {this.state.sortedType === 'alphabetically' ?
                                <span className="sorting-option">alphabetically</span> :
                                <a
                                    href="#"
                                    className="sorting-anchor"
                                    onClick={(e) => this.handleSort(e, 'alphabetically')}
                                    title="Sort Alphabetically"
                                >
                                    alphabetically
                                </a>
                            }
                        </li>
                        <li className="sorting-item country">
                            {this.state.sortedType === 'country' ?
                                <span className="sorting-option">country</span> :
                                <a
                                    href="#"
                                    className="sorting-anchor"
                                    onClick={(e) => this.handleSort(e, 'country')}
                                    title="Sort country"
                                >
                                    country
                                </a>
                            }
                        </li>
                    </ul>
                </div>
                <ul className="contact-list">
                    {this.state.contacts.map((item, i) => {
                        return (
                            <li className="contact-item" key={i}>
                                <NavLink
                                    className="contact-anchor"
                                    activeClassName="selected-contact"
                                    to={{
                                        pathname: `/contact/${item.id}`,
                                        state: { contact: item },
                                    }}
                                >
                                    {item.lastName} {this.sortedByCriteria('alphabetically', item, 'firstName')}
                                    {` / ${item.email}`} ({this.sortedByCriteria('country', item.country, 'name')})
                                </NavLink>
                            </li>
                        );
                    })}
                </ul>
            </div>
        );
    }
}
