import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
    storeIdConst,
    addNewContactEntry,
} from '../store/store';
import { validate } from '../utils/validation/validate';

/**
 * @name AddContactForm
 * @description This is a component used for adding a new contact into the address book.
 * @param {array} countries - An entire array of countries
 * @example
 * <AddContactForm countries={this.props.countries} />
 */
class AddContactForm extends Component {
    static propTypes = {
        countries: PropTypes.array,
        contacts: PropTypes.array,
        addNewContactEntry: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);
        this.state = {
            error: {},
            contactData: {},
        };
    }

    validateAllFields = (contactObj) => {
        return Object.keys(contactObj).filter((key) => key !== 'id' && key !== 'country').forEach((key, i) => {
            setTimeout(() => {
                validate(key, contactObj[key], this.setState.bind(this), this.state.error);
                const hasNoErrors = Object.keys(this.state.error).every((key) => {
                    return this.state.error[key].message === '';
                });
                this.setState(() => {
                    return { hasNoErrors };
                });
                if (hasNoErrors && Object.keys(contactObj).includes('id')) {
                    // save the new entry
                    return this.props.addNewContactEntry(contactObj);
                }

                // if not valid, keep the errors
                return false;
            }, i);
        });
    }

    handleAddNewContact = (e) => {
        e.preventDefault();
        // get all the fields values
        const firstNameValue = ReactDOM.findDOMNode(this.refs.firstName).value;
        const lastNameValue = ReactDOM.findDOMNode(this.refs.lastName).value;
        const emailValue = ReactDOM.findDOMNode(this.refs.email).value;
        const countryValue = ReactDOM.findDOMNode(this.refs.country).value;
        const {
            firstName, lastName, email, country,
        } = this.state.contactData;

        // initial values to validate
        let contactObj = {
            firstName: firstNameValue,
            lastName: lastNameValue,
            email: emailValue,
        };
        // save the contact to the store
        if (firstName && lastName && email && ((country && country.code) || countryValue)) {
            contactObj = {
                // get the maximum id and increment it
                id: this.props.contacts.length ?
                    // if we already have contacts, increment the id of the currently newly added contact
                    (this.props.contacts.map((contact) => contact.id).reduce((a, b) => window.Math.max(a, b))) + 1 :
                    // if we don't have any contacts, start incrementing the contacts from 0 (the length of the array)
                    this.props.contacts.length + 1,
                firstName,
                lastName,
                email,
                country: {
                    code: (country && country.code) || countryValue,
                    name: this.props.countries
                        .filter((item) => item.code === ((country && country.code) || countryValue))[0].name,
                },
            };

            // validate all fields
            this.validateAllFields(contactObj);
        }

        // validate even if the fields are not pre-populated, it makes sense for the UX
        return this.validateAllFields(contactObj);
    }

    handleEditField = (e, field) => {
        let updatedContactData;

        // handle differently the country field
        if (field === 'country') {
            updatedContactData = Object.assign({}, this.state.contactData, {
                [field]: {
                    code: e.target.value,
                    name: this.props.countries.filter((country) => country.code === e.target.value)[0].name,
                },
            });
        } else {
            updatedContactData = Object.assign({}, this.state.contactData, { [field]: e.target.value });
        }

        // update the contact with the new values
        this.setState({ contactData: updatedContactData });

        // get the value again, after the state batching
        setTimeout(() => {
            validate(field, this.state.contactData[field], this.setState.bind(this), this.state.error);
        }, 0);
    }

    render() {
        return (
            <form className="add-contact-form" onSubmit={this.handleAddNewContact}>
                <h3 className="header-subtitle">Add New Contact</h3>
                <fieldset className="add-contact-fieldset">
                    <ul className="contact-details">
                        <li className="contact-details-item contact-first-name">
                            <label className="contact-label" htmlFor="firstName">First Name: (family name)</label>
                            <input
                                type="text"
                                name="firstName"
                                id="firstName"
                                ref="firstName"
                                placeholder="enter your first name"
                                onBlur={(e) => this.handleEditField(e, 'firstName')}
                            />
                        </li>
                        <li className="contact-details-item contact-last-name">
                            <label className="contact-label" htmlFor="lastName">Last Name: (surname)</label>
                            <input
                                type="text"
                                name="lastName"
                                id="lastName"
                                ref="lastName"
                                placeholder="enter your last name"
                                onBlur={(e) => this.handleEditField(e, 'lastName')}
                            />
                        </li>
                        <li className="contact-details-item contact-email">
                            <label className="contact-label" htmlFor="email">Email: </label>
                            <input
                                type="text"
                                name="email"
                                id="email"
                                ref="email"
                                placeholder="enter your email"
                                onBlur={(e) => this.handleEditField(e, 'email')}
                            />
                        </li>
                        <li className="contact-details-item contact-country">
                            <label className="contact-label" htmlFor="country">Country: </label>
                            <select
                                name="country"
                                id="country"
                                ref="country"
                                onChange={(e) => this.handleEditField(e, 'country')}
                            >
                                {this.props.countries.map((country, i) => {
                                    return (
                                        <option value={country.code} key={i}>{country.name}</option>
                                    );
                                })}
                            </select>
                        </li>
                    </ul>
                    <ul className="error-messages">
                        {Object.keys(this.state.error).map((key, i) => {
                            return (
                                <li className="error-item" key={i}>
                                    <span className="error-message">{this.state.error[key].message}</span>
                                </li>

                            );
                        })}
                    </ul>
                    <button className="add-contact-action" type="submit">add contact</button>
                </fieldset>
            </form>
        );
    }
}

export default connect(
    (state) => ({
        countries: state[storeIdConst.STORE_ID].countries,
        contacts: state[storeIdConst.STORE_ID].contacts,
    }),
    (dispatch) => bindActionCreators({
        addNewContactEntry,
    }, dispatch),
)(AddContactForm);
