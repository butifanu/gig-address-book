/**
 * @name camelToWords
 * @description Transform a string from camel case format to capital words.
 * @param {string} string - the string we should operate on
 * @example
 * camelToWords('johnDoe'); // John Doe
 */
export function camelToWords(string) {
    const result = string.replace(/([A-Z])/g, ' $1');
    return `${result.charAt(0).toUpperCase()}${result.slice(1)}`;
}
