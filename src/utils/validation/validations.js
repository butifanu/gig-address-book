/**
 * @name isEmpty
 * @description Checks to see if a provided value is empty string or not.
 * @param {string} value - the value to test against to see if it's empty or not
 * @example
 * isEmpty('John'); // false
 */
export function isEmpty(value) {
    return value === '';
}

/**
 * @name minLength
 * @description Checks to see if the provided value has a minimum required length.
 * @param {string} value - the value to test against to see if it's having the required minimum length
 * @param {number} min - the number representing the minimum length of the string to test
 * @example
 * minLength('Jo', 3); // false
 */
export function minLength(value, min = 2) {
    return value && value.length >= min;
}

/**
 * @name onlyLetters
 * @description Checks to see if a provided value has only letters or not.
 * @param {string} value - the value to test against to see if it has only letters
 * @example
 * onlyLetters('Do3'); // false
 */
export function onlyLetters(value) {
    const regexToMatch = /^[a-zA-Z]+$/i;
    return regexToMatch.test(value);
}

/**
 * @name isEmail
 * @description Checks to see if a provided value has a valid email format.
 * @param {string} value - the value to test against to see if it's a valid email format
 * @example
 * isEmail('john_doe@domain.com'); // true
 */
export function isEmail(value) {
    const regexToMatch = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i; // eslint-disable-line max-len, no-useless-escape
    return regexToMatch.test(value);
}
